import { useTranslation } from 'react-i18next';

export const Navigation = () => {
  const { t, i18n } = useTranslation();

  const changeLanguage = (lang) => {
    i18n.changeLanguage(lang);
  }

  return (
    <nav id='menu' className='navbar navbar-default navbar-fixed-top'>
      <div className='container'>
        <div className='navbar-header'>
          <button
            type='button'
            className='navbar-toggle collapsed'
            data-toggle='collapse'
            data-target='#bs-example-navbar-collapse-1'
          >
            {' '}
            <span className='sr-only'>Toggle navigation</span>{' '}
            <span className='icon-bar' />{' '}
            <span className='icon-bar' />{' '}
            <span className='icon-bar' />{' '}
          </button>
          <a className='navbar-brand page-scroll'
             href={'#page-top'}>
            <img src={'img/logo.png'} alt="Logo"/>
          </a>{' '}
        </div>

        <div
          className='collapse navbar-collapse'
          id='bs-example-navbar-collapse-1'
        >
          <ul className='nav navbar-nav navbar-right'>
            <li>
              <a href={'#features'} className='page-scroll'>
                {t('navigation.features')}
              </a>
            </li>
            <li>
              <a href={'#about'} className='page-scroll'>
                {t('navigation.about')}
              </a>
            </li>
            <li>
              <a href={'#services'} className='page-scroll'>
                {t('navigation.services')}
              </a>
            </li>
            {/*<li>
              <a href={'#portfolio'} className='page-scroll'>
                Gallery
              </a>
            </li>*/}
            {/*<li>
              <a href={'#testimonials'} className='page-scroll'>
                Testimonials
              </a>
            </li>*/}
            {/*<li>
              <a href={'#team'} className='page-scroll'>
                Team
              </a>
            </li>*/}
            <li>
              <a href={'#products'} className='page-scroll'>
                {t('navigation.products')}
              </a>
            </li>
            <li>
              <a href={'#contact'} className='page-scroll'>
                {t('navigation.contact')}
              </a>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                 aria-haspopup="true" aria-expanded="false">
                {t('language')}
              </a>
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <span onClick={() => changeLanguage('en')}
                   className="dropdown-item" href="#">
                  <img src={"img/languages/en.gif"} alt={'En'} />
                  English
                </span>
                <span onClick={() => changeLanguage('am')}
                   className="dropdown-item" href="#">
                  <img src={"img/languages/am.gif"} alt={'Am'} />
                  Հայերեն
                </span>
                <span onClick={() => changeLanguage('ru')}
                   className="dropdown-item" href="#">
                  <img src={"img/languages/ru.gif"} alt={'Ru'} />
                  Русский
                </span>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}
