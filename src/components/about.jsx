import { useTranslation } from 'react-i18next';

export const About = (props) => {
  const {t} = useTranslation();

  return (
      <div id='about'>
        <div className='container'>
          <div className='row'>
            <div className='col-xs-12 col-md-6'>
              {' '}
              <img src={'img/about.jpg'} className='img-responsive' alt='About'/>{' '}
            </div>
            <div className='col-xs-12 col-md-6'>
              <div className='about-text'>
                <h2>{t('navigation.about')}</h2>
                <p>{t('about.paragraph')}</p>
                <h3>{t('about.whyChooseUs')}</h3>
                <div className='list-style'>
                  <div className='col-lg-10 col-sm-10 col-xs-12'>
                    <ul>
                      {props.data
                          ? props.data.Why.map((d, i) => (
                              <li key={`${d}-${i}`}>{t(`about.why.${i}`)}</li>
                          ))
                          : 'loading'}
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  )
}