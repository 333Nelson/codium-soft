import { useTranslation } from 'react-i18next';

export const Header = (props) => {
  const { t } = useTranslation();
  return (
    <header id='header'>
      <div className='intro'>
        <div className='overlay'>
          <div className='container'>
            <div className='row'>
              <div className='col-md-8 col-md-offset-2 intro-text'>
                {/*<img src={props.data ? props.data.img : "loading..."} alt="Logo" />*/}
                <h1>{t('header.title')}</h1>
                <h2>{t('header.subTitle')}</h2>
                <p>{t('header.description')}</p>
                <a
                  href='#features'
                  className='btn btn-custom btn-lg page-scroll'
                >
                  CodiumSoft {t('trainings')}
                </a>{' '}
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  )
}
