import { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import emailjs from 'emailjs-com';

const initialState = {
  name: '',
  email: '',
  message: '',
}

export const Contact = (props) => {
  const {t} = useTranslation();
  const [{ name, email, message }, setState] = useState(initialState);
  const [nameError, setNameError] = useState(false);
  const [emailError, setEmailError] = useState(false);
  const [isDisabledButton, setIsDisabledButton] = useState(false);
  const [successMessage, setSuccessMessage] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target
    if(name === 'name') {setNameError(value === '')}

    if(name === 'email') {setEmailError(value === '')}

    setState((prevState) => ({ ...prevState, [name]: value }))
  }

  const clearState = () => setState({ ...initialState })

  const handleSubmit = (e) => {
    e.preventDefault();
    setNameError(name === '')
    setEmailError(email === '')

    if(name === '' || email === '') return

    setIsDisabledButton(true);
  
    emailjs.sendForm(process.env.REACT_APP_SERVICE_ID, process.env.REACT_APP_TEMPLATE_ID, e.target, process.env.REACT_APP_USER_ID)
      .then((result) => {
          clearState();
          setIsDisabledButton(false);
          setSuccessMessage(true);
      }, (error) => {
          console.log(error.text);
      });
  }

  useEffect(() => {
    setTimeout(() => {setSuccessMessage(false)}, 5000)
  }, [successMessage])

  return (
    <div>
      <div id='contact'>
        <div className='container'>
          <div className='col-md-8'>
            <div className='row'>
              <div className='section-title'>
                <h2>{t('contactUsForm.title')}</h2>
                <p>{t('contactUsForm.description')}</p>
              </div>
              <form name='sentMessage' onSubmit={handleSubmit}>
                <div className='row'>
                  <div className='col-md-6'>
                    <div className='form-group'>
                      <input
                        type='text'
                        value={name}
                        id='name'
                        name='name'
                        className={`form-control ${nameError ? "error" : ""}`}
                        placeholder={t('contactUsForm.name.value')}
                        onChange={handleChange}
                      />
                      <p className='help-block text-danger'>
                        {nameError ? t('contactUsForm.name.error') : ''}
                      </p>
                    </div>
                  </div>
                  <div className='col-md-6'>
                    <div className='form-group'>
                      <input
                        type='text'
                        id='email'
                        value={email}
                        name='email'
                        className={`form-control ${emailError ? "error" : ""}`}
                        placeholder={t('contactUsForm.email.value')}
                        onChange={handleChange}
                      />
                      <p className='help-block text-danger'>
                        {emailError ? t('contactUsForm.email.error') : ''}
                      </p>
                    </div>
                  </div>
                </div>
                <div className='form-group'>
                  <textarea
                    name='message'
                    id='message'
                    value={message}
                    className='form-control'
                    rows='4'
                    placeholder={t('contactUsForm.message.value')}
                    onChange={handleChange}
                  />
                  <p className='help-block text-danger' />
                </div>
                {
                  successMessage && <div id='success' className="text-success">
                    {t('contactUsForm.success message')}
                  </div>
                }
                <button type='submit' className={`btn btn-custom btn-lg ${isDisabledButton ? 'disabled' : ''}`}>
                  {t('contactUsForm.send message')}
                </button>
              </form>
            </div>
          </div>
          <div className='col-md-3 col-md-offset-1 contact-info'>
            <div className='contact-item'>
              <h3>{t('contactUsForm.subTitle')}</h3>
              <p>
                <span>
                  <i className='fa fa-map-marker' /> 
                  {t('contactUsForm.address')}
                </span>
                {props.data ? props.data.address : 'loading'}
              </p>
            </div>
            <div className='contact-item'>
              <p>
                <span>
                  <i className='fa fa-phone' /> 
                  {t('contactUsForm.phone')}
                </span>{' '}
                {props.data ? props.data.phone : 'loading'}
              </p>
            </div>
            <div className='contact-item'>
              <p>
                <span>
                  <i className='fa fa-envelope-o' /> 
                  {t('contactUsForm.email.value')}
                </span>{' '}
                {props.data ? props.data.email : 'loading'}
              </p>
            </div>
          </div>
          <div className='col-md-12'>
            <div className='row'>
              <div className='social'>
                <ul>
                  <li>
                    <a href={props.data ? props.data.facebook : '/'}
                       rel="noopener noreferrer"
                       target={'_blank'}>
                      <i className='fa fa-facebook' />
                    </a>
                  </li>
                  <li>
                    <a href={props.data ? props.data.twitter : '/'}
                       rel="noopener noreferrer"
                       target={'_blank'}>
                      <i className='fa fa-twitter' />
                    </a>
                  </li>
                  <li>
                    <a href={props.data ? props.data.linkedin : '/'}
                       rel="noopener noreferrer"
                       target={'_blank'}>
                      <i className='fa fa-linkedin' />
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id='footer'>
        <div className='container text-center'>
          <p>
            &copy; {new Date().getFullYear()} {t('all rights reserved')} {' '}
            <a href='/'>
              CodiumSoft
            </a>
          </p>
        </div>
      </div>
    </div>
  )
}
